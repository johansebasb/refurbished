So, everything's up. Once you download the repository to start working 
on your own computer do the following:

1. If you already downloaded all the files you're going to see a file called
refurbished_db.sql. It contains the base database installation of magento,
so go to mysql and create a new database called refurbished. 

2. After the database is created, restore the backup using refurbished_db.sql

3. Create an user called ref_admin which password is @refur1 on localhost

4. Important Step for XAMPP: Edi your host file in C:\Windows\System32\drivers\etc and make an alias for IP 127.0.0.1  ie: 127.0.0.1 cdb.localhost.com

5. In Table core_config_data edit "web/unsecure/base_url" & "web/secure/base_url" to point to your local installation

6. Manually delete ALL contents of var/cache

Now I think you are good to go. With this configuration you'll have exactly
the same configuration as in the server for this instantiation of magento.

Anyway, if needed, magento username is refur and password is @refur1

Go in peace my son and may the force be with you!

